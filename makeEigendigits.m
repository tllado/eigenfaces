%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% makeEigendigits()

% Receives an array of training images. Returns their combined eigenvectors and 
% mean.

% written by Travis Llado
% last edited 2017.09.15
% travisllado@utexas.edu

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [eigendigits, meanDigit] = makeEigendigits(trainingImages)
    numImages = size(trainingImages, 3);
    imageRes = size(trainingImages, 1);

    % Combine images into matrix
    A = zeros(imageRes^2, numImages);

    for imageNum = 1:numImages
        A(:, imageNum) = squareToVector(trainingImages(:, :, imageNum));
    end

    % Calculate mean image vector
    meanDigit = mean(A, 2);
    
    % Calculate eigendigits
    [V, D] = eig((A - meanDigit)'*(A - meanDigit));
    V = A*V;

    % Normalize eigendigits
    for columnNum = 1:size(V, 2)
        V(:, columnNum) = normc(V(:, columnNum));
    end

    % Sort eigendigits
    [~, sortOrder] = sort(diag(D), 'descend');

    eigendigits = V;

    for columnNum = 1:size(sortOrder, 1)
        eigendigits(:, columnNum) = V(:, sortOrder(columnNum));
    end
end