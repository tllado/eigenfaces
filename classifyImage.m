%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% classifyImage

% Receives an image and eigenface data for several classes. Returns the class in
% which the image fits.

% written by Travis Llado
% last edited 2017.09.15
% travisllado@utexas.edu

function supposedClass = classifyImage(image, eigendigits, meanVectors)
    norms = zeros(10, 1);
    
    for thisDigit = 1:10
        reconstructedImage = reconstructImage(image, eigendigits(:, :, thisDigit), meanVectors(:, thisDigit));
        norms(thisDigit) = norm(squareToVector(image) - squareToVector(reconstructedImage));
    end

    [~, supposedClass] = min(norms);
    supposedClass = mod(supposedClass, 10);
end