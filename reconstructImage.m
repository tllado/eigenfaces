%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% reconstructImage

% Receives an image and eigenface data for a class of images. Returns the image
% created by projecting the original across the eigendata.

% written by Travis Llado
% last edited 2017.09.15
% travisllado@utexas.edu

function reconstructedImage = reconstructImage(originalImage, eigendigits, meanVector)
    k = normc((normc(squareToVector(originalImage) - meanVector)'*eigendigits)');
    
    reconstructedImage = vectorToSquare(eigendigits*k + normc(meanVector));
end